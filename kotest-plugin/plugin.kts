import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiClass
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiJavaFile
import com.intellij.psi.PsiMethod
import com.intellij.psi.util.PsiTreeUtil
import liveplugin.*
import liveplugin.implementation.common.IdeUtil.showInputDialog
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.Socket
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.atomic.AtomicBoolean
import java.util.regex.Pattern

// depends-on-plugin com.intellij.java

val digitPattern: Pattern = Pattern.compile("[1-9]\\d*")
var client: ReplClient? = null
    set(value) {
        field?.close()
        field = value
    }

pluginDisposable.whenDisposed {
    client = null
}

fun askPortIfNeeded(project: Project?) {
    val testedClient = client
    if (testedClient == null) {
        val portValue = project.showInputDialog(
                "Configure REPL server port",
                "Title",
                liveplugin.implementation.common.inputValidator { value ->
                    if (digitPattern.matcher(value).matches()) null else "Expecting a positive number"
                },
                "7777")
        client = ReplClient(Integer.parseInt(portValue))
    } else if (!testedClient.isConnected()) {
        try {
            val newClient = ReplClient(testedClient.getPort())
            client = newClient
        } catch (e: Exception) {
            show("Client disconnected and removed")
            client = null
            testedClient.close()
        }
    }
}

fun getCurrentElement(event: AnActionEvent): PsiElement? {
    val editor = event.editor
    val project = event.project
    val psiFile = event.psiFile
    if (editor != null && project != null && psiFile != null && psiFile is PsiJavaFile) {
        val position: Int = editor.caretModel.offset
        return psiFile.findElementAt(position)
    } else {
        return null;
    }
}

registerAction("repl-exec-test", keyStroke = "ctrl alt K", disposable = pluginDisposable) { event ->
    getCurrentElement(event)?.let { currentElement ->
        val currentMethod = PsiTreeUtil.getParentOfType(currentElement, PsiMethod::class.java)
        if (currentMethod != null) {
            val currentClass = currentMethod.containingClass?.qualifiedName
            askPortIfNeeded(project)
            val magicName = "exec:${currentClass}#${currentMethod.name}"
            val success = client!!.send(magicName)
            if (!success) {
                show("Message not sent :(")
            }
        } else {
            show("No test")
        }
    }
}

registerAction("repl-test-element", keyStroke = "ctrl alt X", disposable = pluginDisposable) { event ->
    getCurrentElement(event)?.let { currentElement ->
        val currentMethod = PsiTreeUtil.getParentOfType(currentElement, PsiMethod::class.java)
        val currentClass = PsiTreeUtil.getParentOfType(currentElement, PsiClass::class.java)
        val javaFile = event.psiFile as PsiJavaFile?
        val packageName = javaFile?.packageStatement?.packageName
        val command = if (currentMethod != null) {
            val className = currentMethod.containingClass?.qualifiedName
            "test:${className}#${currentMethod.name}"
        } else if (currentClass != null) {
            "test:${currentClass.qualifiedName}"
        } else if (packageName != null) {
            "test-package:$packageName"
        } else {
            show("No test / class / package")
            null
        }
        if (command != null) {
            askPortIfNeeded(project)
            val success = client!!.send(command)
            if (!success) {
                show("Message not sent :(")
            }
        }
    }
}

registerAction("Reconnect to JRepl server", disposable = pluginDisposable) { event ->
    event.project?.let { project ->
        client = null
        askPortIfNeeded(project)
    }
}

registerAction("Send smart JRepl command", disposable = pluginDisposable) { event ->
    event.project?.let { project ->
        askPortIfNeeded(project)
        val command = project.showInputDialog(
                "Command to send",
                "Title")
        command?.let { cmd -> client!!.send(cmd) }
    }
}

show("Plugin `JREPL` started at " + java.time.Instant.now())

class ReplClient(port: Int) {

    private var clientSocket: Socket
    private var output: PrintWriter
    private val reader: BufferedReader
    private val messageQueue = ArrayBlockingQueue<String>(10)
    private val flag = AtomicBoolean(true)
    private val thread: Thread

    init {
        this.clientSocket = Socket("localhost", port)
        this.output = PrintWriter(this.clientSocket.getOutputStream(), true)
        this.reader = BufferedReader(InputStreamReader(this.clientSocket.getInputStream()))
        this.thread = Thread({
            while (true) {
                val msg = this.messageQueue.take()
                if (this.flag.get()) {
                    sendCommand(msg)
                }
            }
        }, "message-sender")
        this.thread.start()
    }

    fun getPort(): Int = this.clientSocket.port

    fun isConnected(): Boolean {
        return !this.clientSocket.isClosed
    }

    fun send(command: String): Boolean {
        return this.messageQueue.offer(command)
    }

    private fun sendCommand(msg: String) {
        this.output.println(msg)
        this.output.flush()
        val response = this.reader.readLine()
        if ("ok" != response) {
            show("REPL Server returned $response")
        }
    }

    fun close() {
        this.thread.interrupt()
        this.flag.set(false)
        this.messageQueue.clear()
        this.messageQueue.offer("--the end--") // To trigger the queue and unlock the thread

        this.reader.close()
        this.output.close()
        this.clientSocket.close()
    }
}
