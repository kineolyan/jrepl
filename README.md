Java REPL extension
=======

Adding JREPL to your project.
----

The REPL server is defined by the class `io.gitlab.kineolyan.jrepl.ReplServer.java`. It defines the main method starting and
running the REPL, reading commands from stdin or a socket.<br/>

To integrate it in your IDE, the best way is to run this class within the classpath of your project.<br/>
Because JREPL offers tools to run tests without restarting your session, it is recommended to create a class
calling `ReplServer` inside your test directory.

```java
import io.gitlab.kineolyan.jrepl.ReplServer;

class MyReplInsideTests extends ReplServer {
}
```

This "tricks" the IDE - you can as a configuration - to run the class with the project test classpath.<br/>
This class can even be the base of some of your REPL session variables.

```java
import io.gitlab.kineolyan.jrepl.ReplCode;
import io.gitlab.kineolyan.jrepl.ReplServer;

class MyReplInsideTests {

    public static void main(String[] args) {
        ReplServer.main(args);
    }

    @ReplCode
    static void inspectValue() {
        var value = ReplApi.value("my-key").as < List < String >> ();
        // ... do something with the value
    }
}
```

Usage
---

`<class name>#<method name>` run the referenced method in the REPL
`!!` re-run the last method

TODOs
----

- [x] For public static methods
- [x] For public methods for objects with no-arg constructors
- [x] Support basic redo
- [x] Support history of results
- [x] Support `protected` methods
- [x] Support `package-private` methods
- [x] Support `private` methods
- [ ] Support inherited methods
- [x] Add a help message
- [ ] Improve the display of the server (don't use logger for output)
- [x] Add long-running context
- [x] Create an IntelliJ LivePlugin to send the command from the IDE
- [x] Run JUnit tests directly
- [x] Work with JRebel