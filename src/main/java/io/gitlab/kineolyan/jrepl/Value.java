package io.gitlab.kineolyan.jrepl;

public interface Value {
  <T> T as();

  <T> T as(Class<T> valueClass);

  default int asInt() {
    return as(Number.class).intValue();
  }

  default long asLong() {
    return as(Number.class).longValue();
  }

  default float asFloat() {
    return as(Number.class).floatValue();
  }

  default double asDouble() {
    return as(Number.class).floatValue();
  }
}
