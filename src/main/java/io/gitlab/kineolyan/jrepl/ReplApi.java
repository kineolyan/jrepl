package io.gitlab.kineolyan.jrepl;

import io.gitlab.kineolyan.jrepl.internal.ReplSession;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

public final class ReplApi {

  private ReplApi() {}

  private static final AtomicReference<ReplSession> instance = new AtomicReference<>();

  static synchronized void bind(final ReplSession server) {
    Objects.requireNonNull(server);
    ReplApi.instance.updateAndGet(
        current -> {
          if (current == null) {
            return server;
          } else {
            throw new IllegalStateException("Server already bound");
          }
        });
  }

  static synchronized void unbind(final ReplSession server) {
    Objects.requireNonNull(server);
    ReplApi.instance.updateAndGet(
        current -> {
          if (current == server) {
            return null;
          } else {
            throw new IllegalStateException("Passed server not bound to any session");
          }
        });
  }

  private static ReplSession getCurrentSession() {
    return Objects.requireNonNull(ReplApi.instance.get(), "No defined session");
  }

  public static Value last(final int position) {
    return getCurrentSession().last(position);
  }

  public static Optional<Throwable> lastError() {
    return getCurrentSession().lastError();
  }

  public static Value value(final String variableName) {
    return getCurrentSession().value(variableName);
  }

  public static FnValue variable(final String variableName) {
    return getCurrentSession().variable(variableName);
  }

  public static void def(final String variableName, final Object value) {
    getCurrentSession().def(variableName, value);
  }

  public static void undef(final String variableName) {
    getCurrentSession().undef(variableName);
  }

  public static Collection<String> variableNames() {
    return getCurrentSession().variableNames();
  }

  public static void undefAllVariables() {
    getCurrentSession().undefAllVariables();
  }
}
