package io.gitlab.kineolyan.jrepl;

import io.gitlab.kineolyan.jrepl.internal.SocketReader;
import io.gitlab.kineolyan.jrepl.internal.MethodEvaluator;
import io.gitlab.kineolyan.jrepl.internal.ReplMethod;
import io.gitlab.kineolyan.jrepl.internal.ReplSession;

import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

final class SandboxTesting {
  private SandboxTesting() {}

  public static void levelControl() {
    Stream.of(ReplServer.class, SocketReader.class)
        .map(Class::getName)
        .map(Logger::getLogger)
        .forEach(logger -> logger.setLevel(Level.FINEST));
  }

  public static void helloJRebel() {
    System.out.println("SandboxTesting.helloJRebel");
  }

  public static void oneMoreTest() {
    System.out.println("it seems so");
  }

  static void sandbox() {
    ReplApi.def("a", 1);
    ReplApi.def("b", 23.4d);
    System.out.println(ReplApi.variableNames());
    ReplApi.undefAllVariables();
    System.out.println(ReplApi.variableNames());
  }

  public static void testNewMethod() {
    testNewMethod("io.kineolyan.jrepl.SandboxTesting.Actor#doAction");
    //    testNewMethod("io.kineolyan.jrepl.ReplServer.Actor#greet");
    //    testNewMethod("io.kineolyan.jrepl.ReplServer.Actor#protectedMethod");
    //    testNewMethod("io.kineolyan.jrepl.ReplServer.Actor#staticMethod");
    //    testNewMethod("io.kineolyan.jrepl.ReplServer.Actor#privateMethod");
  }

  @ReplValue("magic-value")
  static int aConstant() {
    return 42;
  }

  @ReplValue(value = "constant-value", once = true)
  static double pi() {
    return Math.PI;
  }

  static void readConstant() {
    System.out.println("ReplServer.readConstant value = " + ReplApi.value("magic-value").as());
  }

  static void callingHistoryResultsAndValues() {
    System.out.println("ReplServer.testNewMethod: error " + ReplApi.lastError());
    System.out.println("ReplServer.testNewMethod: res[-1] " + ReplApi.last(1).as());
  }

  static void definingAVariable() {
    ReplApi.def("value", 1);
    System.out.println("value = " + ReplApi.value("value").as());
  }

  static void varVsVal() {
    ReplApi.def("fn", (Supplier<Long>) () -> 12L);
    final var fnVal = ReplApi.value("fn");
    final var fnVar = ReplApi.variable("fn");
    System.out.println("Calling " + fnVal.as(Supplier.class).get() + " | " + fnVar.invoke());

    ReplApi.def("fn", (Supplier<String>) () -> "the-value");
    System.out.println("Calling " + fnVal.as(Supplier.class).get() + " | " + fnVar.invoke());
    // fnVar result has **changed** but fnVal has **not**
  }

  public static class Actor {

    public static int doAction() {
      return 1;
    }

    public String greet() {
      return "Hello friend";
    }

    protected String protectedMethod() {
      return "protected-ok";
    }

    String staticMethod() {
      return "package-private-ok";
    }

    private String privateMethod() {
      return "private-ok";
    }
  }

  private static void testNewMethod(final String input) {
    final var method = ReplMethod.fromRef(input);
    new MethodEvaluator().evaluate(ReplSession.create(), method);
  }
}
