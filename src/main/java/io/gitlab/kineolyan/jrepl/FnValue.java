package io.gitlab.kineolyan.jrepl;

public interface FnValue extends Value {

  Object invoke(Object... args);
}
