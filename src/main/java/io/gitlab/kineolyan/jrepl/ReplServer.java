package io.gitlab.kineolyan.jrepl;

import io.gitlab.kineolyan.jrepl.internal.*;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class ReplServer {

  public static void main(final String... args) {
    java.util.logging.Logger.getLogger(ReplServer.class.getName()).setLevel(Level.ALL);
    final CommandReader reader;
    if (args.length > 0) {
      final var socketReader = SocketReader.onPort(Integer.parseInt(args[0]));
      socketReader.open();
      reader = socketReader;
      logger.log(
          Level.INFO,
          "Server started reading socket on port {0}",
          new Object[] {socketReader.getPort()});
    } else {
      reader = StdinReader.create();
      logger.info("Server started reading STDIN");
    }
    final var server = new ReplServer(reader);
    ReplApi.bind(server.getSession());
    try {
      server.run();
    } finally {
      ReplApi.unbind(server.getSession());
    }
  }

  private static final Logger logger = Logger.getLogger(ReplServer.class.getName());

  private final CommandReader reader;

  private final ReplInterpreter interpreter;
  private final ReplEvaluator evaluator;

  private ReplServer(final CommandReader reader) {
    this.reader = reader;
    this.interpreter = ReplInterpreter.create();
    this.evaluator = ReplEvaluator.create();
  }

  private ReplSession getSession() {
    return this.evaluator.getSession();
  }

  private void run() {
    while (!Thread.currentThread().isInterrupted()) {
      runLoop();
    }
  }

  private void runLoop() {
    final var input = readInput();
    Optional<Command> command;
    try {
      command = Optional.ofNullable(this.interpreter.interpret(input));
    } catch (final RuntimeException e) {
      logger.log(Level.WARNING, "Error parsing `" + input + "`", e);
      command = Optional.empty();
    }
    command.ifPresent(
        cmd -> {
          try {
            this.evaluator.evaluate(cmd);
          } catch (final RuntimeException e) {
            logger.log(Level.WARNING, "Error evaluating `" + cmd + "`", e);
          }
        });
  }

  private String readInput() {
    logger.finer("Waiting for input");
    final var input = this.reader.readCommand();
    logger.log(Level.FINER, "Read input: <{0}>", new Object[] {input});
    return input;
  }
}
