package io.gitlab.kineolyan.jrepl;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation marking methods used only for REPL exploration.
 *
 * <p>This mostly serves as a hint for IDEs not to report the methods as useless for not being used.
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface ReplCode {}
