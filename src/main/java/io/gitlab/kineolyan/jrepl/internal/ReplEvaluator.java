package io.gitlab.kineolyan.jrepl.internal;

import io.gitlab.kineolyan.jrepl.ReplApi;
import io.gitlab.kineolyan.jrepl.Value;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.junit.platform.engine.UniqueId;
import org.junit.platform.launcher.listeners.TestExecutionSummary;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class ReplEvaluator {

  private static final Logger logger = Logger.getLogger(ReplEvaluator.class.getName());

  private final RingBuffer<ReplMethod> history;
  @Getter private final ReplSession session;

  public static ReplEvaluator create() {
    return new ReplEvaluator(RingBuffer.ofSize(5), ReplSession.create());
  }

  public void evaluate(final Command command) {
    if (command instanceof Command.PrintHelp) {
      printHelpMessage();
    } else if (command instanceof Command.ShowLastError) {
      final var lastError = this.session.lastError().orElse(null);
      if (lastError == null) {
        logger.info("No error recorded");
      } else {
        logger.log(Level.INFO, "Last error", lastError);
      }
    } else if (command instanceof Command.ShowResult r) {
      Optional<Value> historicalResult;
      try {
        historicalResult = Optional.of(this.session.last(r.position()));
      } catch (final RuntimeException e) {
        logger.log(
            Level.SEVERE, "Cannot access historical the " + r.position() + " nth last result", e);
        historicalResult = Optional.empty();
      }
      historicalResult.map(Value::as).ifPresent(this::reportResult);
    } else if (command instanceof Command.ListVariables) {
      if (logger.isLoggable(Level.INFO)) {
        final var names = ReplApi.variableNames();
        final String message;
        if (names.isEmpty()) {
          message = "No variables currently defined";
        } else {
          final var builder = new StringBuilder("List of REPL variables");
          names.forEach(name -> builder.append("\n - ").append(name));
          message = builder.toString();
        }
        logger.log(Level.INFO, message);
      }
    } else if (command instanceof Command.UndefVariables) {
      ReplApi.undefAllVariables();
      logger.info("All variables defined by the REPL session have been removed");
    } else if (command instanceof Command.RunHistory run) {
      Optional<ReplMethod> historicalCommand;
      try {
        historicalCommand = Optional.of(this.history.last(run.position()));
      } catch (final RuntimeException e) {
        logger.log(
            Level.SEVERE,
            "Cannot find the " + run.position() + " nth historical historicalCommand",
            e);
        historicalCommand = Optional.empty();
      }
      historicalCommand.ifPresent(this::runLoopFor);
    } else if (command instanceof Command.RunTest action) {
      // TODO how to add this command to the history
      runLoopFor(action.test());
    } else if (command instanceof Command.Evaluate action) {
      this.history.push(action.method());
      runLoopFor(action.method());
    } else {
      logger.log(Level.SEVERE, "Unknown command {0}", new Object[] {command});
    }
  }

  private void runLoopFor(final ReplMethod method) {
    try {
      final var result = new MethodEvaluator().evaluate(this.session, method);
      this.session.pushResult(result);
      reportResult(result);
    } catch (final RuntimeException error) {
      this.session.pushError(error);
      throw error;
    }
  }

  private void runLoopFor(final ReplTest test) {
    try {
      val summary = new JunitTestEvaluator().evaluate(this.session, test);
      reportTestExecution(summary);
    } catch (final RuntimeException error) {
      this.session.pushError(error);
      throw error;
    }
  }

  private void reportResult(final Object result) {
    if (result == null) {
      logger.info("Produced null");
    } else {
      logger.log(
          Level.INFO,
          "Produced {0} (class: {1})",
          new Object[] {result, result.getClass().getName()});
    }
  }

  private void reportTestExecution(final TestExecutionSummary summary) {
    if (!logger.isLoggable(Level.INFO)) {
      return;
    }

    val utf8 = StandardCharsets.UTF_8;
    try (val buffer = new ByteArrayOutputStream();
        val printer = new PrintStream(buffer, true, utf8)) {
      printer.printf(
          "%d tests: %d OK / %d KO%n",
          summary.getTestsFoundCount(),
          summary.getTestsSucceededCount(),
          summary.getTestsFailedCount());
      summary
          .getFailures()
          .forEach(
              failure -> {
                printer.println(
                    failure.getTestIdentifier().getUniqueIdObject().getSegments().stream()
                        .map(UniqueId.Segment::getValue)
                        .collect(Collectors.joining(" :: ")));
                failure.getException().printStackTrace(printer);
              });
      logger.log(Level.INFO, buffer.toString(utf8));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void printHelpMessage() {
    System.out.println(
        """
Help to JREPL
=============

Magic variables:
*1 - Returns the result of the last evaluation
*<n> - Returns the result of the last n-th evaluation (ex: *2)
*e - Prints the last error

Smart commands:
help - Prints this message
!! - Re-runs the last evaluation
!<n> - Re-runs the last n-th evaluation
v* - Prints the list of all variables recorded by this REPL session
v! - Deletes all variables recorded by this REPL session
"""
            .stripIndent());
  }
}
