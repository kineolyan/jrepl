package io.gitlab.kineolyan.jrepl.internal;

public interface CommandReader extends AutoCloseable {

  String readCommand();
}
