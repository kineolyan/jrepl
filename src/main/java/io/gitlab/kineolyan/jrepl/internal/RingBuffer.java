package io.gitlab.kineolyan.jrepl.internal;

import java.util.AbstractList;
import java.util.Objects;

public final class RingBuffer<E> extends AbstractList<E> {

  private final Object[] buffer;
  private int position;
  private int size;

  private RingBuffer(final int size) {
    this.buffer = new Object[size];
    this.position = 0;
    this.size = 0;
  }

  public static <E> RingBuffer<E> ofSize(final int size) {
    return new RingBuffer<>(size);
  }

  @Override
  public E get(final int index) {
    if (index < 0 || index >= this.size) {
      throw new IndexOutOfBoundsException(index);
    }
    final int readPosition;
    if (this.size == this.buffer.length) {
      readPosition = (this.position + index) % this.buffer.length;
    } else {
      readPosition = index;
    }
    @SuppressWarnings("unchecked")
    final var item = (E) this.buffer[readPosition];
    return item;
  }

  public void push(final E item) {
    Objects.requireNonNull(item);
    this.buffer[this.position] = item;
    this.position = (this.position + 1) % this.buffer.length;
    if (this.size < this.buffer.length) {
      this.size += 1;
    }
  }

  public E last() {
    return last(1);
  }

  public E last(final int index) {
    return get(this.size - index);
  }

  @Override
  public int size() {
    return this.size;
  }
}
