package io.gitlab.kineolyan.jrepl.internal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntSupplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import lombok.val;

public class SocketReader implements CommandReader, AutoCloseable {

  private static final Logger logger = Logger.getLogger(SocketReader.class.getName());

  @Getter private final int port;

  private final BlockingQueue<String> commandQueue;
  private ServerSocket serverSocket;
  private final Collection<Client> clients;

  private Thread connectionThread;

  private AtomicBoolean threadSwitch;

  private final IntSupplier threadIds = new AtomicInteger(0)::incrementAndGet;

  private SocketReader(final int port) {
    this.port = port;
    this.commandQueue = new ArrayBlockingQueue<>(10);
    this.clients = Collections.synchronizedCollection(new ArrayList<>());
  }

  public static SocketReader onPort(final int port) {
    return new SocketReader(port);
  }

  public void open() {
    if (this.serverSocket != null) {
      throw new IllegalStateException("Server already open");
    }
    final ServerSocket server;
    try {
      server = new ServerSocket(this.port);
    } catch (final IOException e) {
      throw new IllegalStateException("Cannot open a server on port " + this.port, e);
    }
    this.serverSocket = server;
    this.threadSwitch = new AtomicBoolean(true);
    this.connectionThread = new Thread(this::handleNewConnection, "repl-connection");
    this.connectionThread.start();
  }

  private void handleNewConnection() {
    while (this.threadSwitch.get()) {
      try {
        final var connection = this.serverSocket.accept();
        startClientThread(connection);
      } catch (final IOException e) {
        throw new RuntimeException("Failed accepting a connection", e);
      }
    }
  }

  private void startClientThread(final Socket clientSocket) {
    final var clientId = this.threadIds.getAsInt();
    logger.log(
        Level.FINEST,
        "New connection received. Creating a handler labelled {}",
        new Object[] {clientId});
    final var thread =
        new Thread(() -> this.runClientThread(clientId, clientSocket), "client-thread-" + clientId);
    this.clients.add(new Client(clientSocket, thread));
    logger.log(Level.FINEST, "Client {0} created", new Object[] {clientId});
    thread.start();
    logger.log(Level.FINER, "New client {0} started", new Object[] {clientId});
  }

  private void runClientThread(final int clientId, final Socket clientSocket) {
    try (val in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        final var out = new PrintWriter(clientSocket.getOutputStream(), true)) {
      while (this.threadSwitch.get()) {
        // TODO kill the thread if the client is closed
        final var input =
            readLine(in)
                .orElseThrow(
                    () ->
                        new IllegalStateException(
                            "Too many empty messages received for client " + clientId));
        if (input.isBlank()) {
          logger.log(
              Level.FINER, "Ignoring blank input received by client {0}", new Object[] {clientId});
        } else if ("ok?".equals(input)) {
          logger.finer("Ping message received");
        } else {
          final var success = this.commandQueue.offer(input);
          if (success) {
            logger.log(
                Level.FINER, "Value received for client {0}: {1}", new Object[] {clientId, input});
          } else {
            logger.log(
                Level.WARNING,
                "Value received for client {} but dropped {}",
                new Object[] {clientId, input});
          }
        }
        out.println("ok");
      }
    } catch (final IOException e) {
      throw new RuntimeException("Failed accepting a connection", e);
    }
  }

  private Optional<String> readLine(final BufferedReader reader) throws IOException {
    for (int i = 0; i < 1000; i += 1) {
      final var input = reader.readLine();
      if (input != null && !input.isBlank()) {
        return Optional.of(input);
      }
    }
    return Optional.empty();
  }

  @Override
  public void close() throws IOException {
    this.threadSwitch.set(false);
    this.connectionThread.interrupt();
    final List<Client> registeredClients;
    synchronized (this.clients) {
      registeredClients = List.copyOf(this.clients);
      this.clients.clear();
    }
    for (val client : registeredClients) {
      if (client.thread().isAlive()) {
        client.thread().interrupt();
      }
      client.socket().close();
    }
    serverSocket.close();
  }

  @Override
  public String readCommand() {
    try {
      return this.commandQueue.take();
    } catch (final InterruptedException e) {
      Thread.currentThread().interrupt();
      throw new RuntimeException("Interrupted while waiting for a command", e);
    }
  }

  private record Client(Socket socket, Thread thread) {}
}
