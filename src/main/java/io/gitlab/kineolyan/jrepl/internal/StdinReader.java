package io.gitlab.kineolyan.jrepl.internal;

import java.io.InputStream;
import java.util.Scanner;

public final class StdinReader implements CommandReader {

  private final Scanner reader;

  StdinReader(final InputStream input) {
    this.reader = new Scanner(input);
  }

  public static StdinReader create() {
    return new StdinReader(System.in);
  }

  @Override
  public String readCommand() {
    for (int i = 0; i < 1000; i += 1) {
      final var input = this.reader.nextLine();
      if (!input.isBlank()) {
        return input;
      }
    }
    throw new IllegalStateException("Too many blank lines received");
  }

  @Override
  public void close() {
    // Nothing to do
  }
}
