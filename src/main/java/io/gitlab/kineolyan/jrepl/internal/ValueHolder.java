package io.gitlab.kineolyan.jrepl.internal;

import io.gitlab.kineolyan.jrepl.Value;

public record ValueHolder(Object value) implements Value {

  @SuppressWarnings("unchecked")
  @Override
  public <T> T as() {
    return (T) this.value;
  }

  @Override
  public <T> T as(final Class<T> valueClass) {
    if (valueClass.isInstance(this.value)) {
      return valueClass.cast(this.value);
    } else {
      throw new IllegalArgumentException();
    }
  }
}
