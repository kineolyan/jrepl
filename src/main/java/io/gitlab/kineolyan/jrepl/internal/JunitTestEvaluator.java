package io.gitlab.kineolyan.jrepl.internal;

import io.gitlab.kineolyan.jrepl.ReplApi;
import io.gitlab.kineolyan.jrepl.ReplCode;
import io.gitlab.kineolyan.jrepl.ReplValue;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import lombok.val;
import org.junit.platform.engine.DiscoverySelector;
import org.junit.platform.engine.UniqueId;
import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import org.junit.platform.launcher.listeners.TestExecutionSummary;

public class JunitTestEvaluator {

  private static final Logger logger = Logger.getLogger(ReplEvaluator.class.getSimpleName());

  public TestExecutionSummary evaluate(final ReplSession session, final ReplTest method) {
    val selector = createSelector(method);
    val request = LauncherDiscoveryRequestBuilder.request().selectors(selector).build();

    SummaryGeneratingListener listener = new SummaryGeneratingListener();

    try (val junitSession = LauncherFactory.openSession()) {
      Launcher launcher = junitSession.getLauncher();
      launcher.registerTestExecutionListeners(listener);
      launcher.execute(request);
      // Alternatively, execute the request directly
      // TestPlan testPlan = launcher.discover(request);
      // launcher.execute(testPlan);
    }

    return listener.getSummary();
  }

  static DiscoverySelector createSelector(final ReplTest test) {
    if (test instanceof ReplTest.TestedPackage) {
      return DiscoverySelectors.selectPackage(((ReplTest.TestedPackage) test).packageName());
    }
    if (test instanceof ReplTest.TestedClass) {
      return DiscoverySelectors.selectClass(((ReplTest.TestedClass) test).className());
    } else if (test instanceof ReplTest.TestedMethod) {
      val testedMethod = (ReplTest.TestedMethod) test;
      return DiscoverySelectors.selectMethod(testedMethod.className(), testedMethod.methodName());
    } else {
      throw new UnsupportedOperationException();
    }
  }

  @ReplValue("test-summary")
  static TestExecutionSummary exploreApi() {
    return new JunitTestEvaluator()
        .evaluate(
            null,
            new ReplTest.TestedMethod("io.kineolyan.jrepl.internal.TestRingBuffer", "testEmpty"));
  }

  @ReplCode
  static void buildSummary() {
    val summary = ReplApi.value("test-summary").as(TestExecutionSummary.class);
    System.out.println(
        summary.getTestsFoundCount()
            + " tests: "
            + summary.getTestsSucceededCount()
            + " OK / "
            + summary.getTestsFailedCount()
            + " KO");
    summary
        .getFailures()
        .forEach(
            failure -> {
              System.err.println(
                  failure.getTestIdentifier().getUniqueIdObject().getSegments().stream()
                      .map(UniqueId.Segment::getValue)
                      .collect(Collectors.joining(" :: ")));
              failure.getException().printStackTrace(System.err);
            });
  }
}
