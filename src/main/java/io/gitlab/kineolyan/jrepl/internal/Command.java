package io.gitlab.kineolyan.jrepl.internal;

import lombok.NonNull;

public sealed interface Command
    permits Command.Evaluate,
        Command.RunTest,
        Command.ListVariables,
        Command.RunHistory,
        Command.ShowLastError,
        Command.ShowResult,
        Command.UndefVariables,
        Command.PrintHelp {

  record PrintHelp() implements Command {}

  record RunHistory(int position) implements Command {
    public RunHistory {
      if (position <= 0) {
        throw new IllegalArgumentException("Negative or null position. Got " + position);
      }
    }
  }

  record ShowResult(int position) implements Command {
    public ShowResult {
      if (position <= 0) {
        throw new IllegalArgumentException("Negative or null position. Got " + position);
      }
    }
  }

  record ShowLastError() implements Command {}

  record ListVariables() implements Command {}

  record UndefVariables() implements Command {}

  record Evaluate(@NonNull ReplMethod method) implements Command {}

  record RunTest(@NonNull ReplTest test) implements Command {}
}
