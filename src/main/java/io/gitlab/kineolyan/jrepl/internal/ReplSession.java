package io.gitlab.kineolyan.jrepl.internal;

import io.gitlab.kineolyan.jrepl.FnValue;
import io.gitlab.kineolyan.jrepl.ReplServer;
import io.gitlab.kineolyan.jrepl.Value;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.val;

public final class ReplSession {
  private static final Logger logger = Logger.getLogger(ReplServer.class.getName());

  private final RingBuffer<Value> results;
  private Throwable error;
  private final Map<String, ValueHolder> variables;

  private ReplSession() {
    this.results = RingBuffer.ofSize(5);
    this.error = null;
    this.variables = new HashMap<>();
  }

  public static ReplSession create() {
    return new ReplSession();
  }

  public Collection<String> variableNames() {
    return List.copyOf(this.variables.keySet());
  }

  public boolean defined(final String variableName) {
    return this.variables.containsKey(variableName);
  }

  public void def(final String variableName, final Object value) {
    final var previous = this.variables.put(variableName, new ValueHolder(value));
    if (logger.isLoggable(Level.FINER)) {
      final var template =
          previous == null
              ? "New variable created: {0} = {1} (class {2})"
              : "Variable updated: {0} = {1} (class {2})";
      logger.log(
          Level.FINER,
          template,
          new Object[] {variableName, value, value == null ? "N/A" : value.getClass()});
    }
  }

  public void undef(final String variableName) {
    val value = this.variables.remove(variableName);
    if (value == null) {
      throw new IllegalArgumentException("Unknown variable `" + variableName + "`");
    } else {
      dropValue(variableName, value);
    }
  }

  public void undefAllVariables() {
    logger.finest("Removing all variables defined by the REPL session");
    this.variables.forEach(this::dropValue);
    this.variables.clear();
    logger.finer("All variables defined by the REPL session removed");
  }

  private void dropValue(final String variableName, final ValueHolder valueHolder) {
    logger.log(Level.FINER, "Variable {0} removed", new Object[] {variableName});
    val value = valueHolder.value();
    if (value instanceof AutoCloseable) {
      try {
        ((AutoCloseable) value).close();
      } catch (final Exception e) {
        logger.log(
                Level.FINE,
                "Failed to close the value of var `{0}` (type {1})",
                new Object[] {variableName, value.getClass()});
      }
    }
  }

  public Value value(final String variableName) {
    return Objects.requireNonNull(
        this.variables.get(variableName), () -> "Unknown variable `" + variableName + "`");
  }

  public FnValue variable(final String variableName) {
    return new FnValue() {
      private Value get() {
        return ReplSession.this.value(variableName);
      }

      @Override
      public Object invoke(Object... args) {
        final var value = get().as();
        if (value instanceof Runnable r) {
          r.run();
          return null;
        } else if (value instanceof Callable<?> c) {
          try {
            return c.call();
          } catch (final Exception e) {
            throw new RuntimeException(e);
          }
        } else if (value instanceof Supplier<?> s) {
          return s.get();
        } else if (value == null) {
          throw new IllegalStateException("Cannot invoke the null value");
        } else {
          // Last try, let's look for a single public test without args
          final var noArgMethods =
              Arrays.stream(value.getClass().getMethods())
                  .filter(m -> Modifier.isPublic(m.getModifiers()))
                  .filter(m -> m.getParameterCount() == 0)
                  .toList();
          return switch (noArgMethods.size()) {
            case 0 -> throw new IllegalStateException(
                "Object without public no-arg methods to call as function. Inspected: " + value);
            case 1 -> {
              // TODO extract a utility test to invoke test without checked exceptions.
              try {
                yield noArgMethods.get(0).invoke(value);
              } catch (IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException(e);
              }
            }
            default -> throw new IllegalStateException(
                "Object without too many public no-arg methods to call it as function. Inspected: "
                    + value);
          };
        }
      }

      @Override
      public <T> T as() {
        return get().as();
      }

      @Override
      public <T> T as(final Class<T> valueClass) {
        return get().as(valueClass);
      }
    };
  }

  public void pushResult(final Object value) {
    this.results.push(new ValueHolder(value));
  }

  public void pushError(final Throwable value) {
    this.error = value;
  }

  public Value last(final int position) {
    return this.results.last(position);
  }

  public Optional<Throwable> lastError() {
    return Optional.ofNullable(this.error);
  }
}
