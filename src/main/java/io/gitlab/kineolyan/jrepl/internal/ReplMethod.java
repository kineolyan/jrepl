package io.gitlab.kineolyan.jrepl.internal;

import lombok.val;import java.util.regex.Pattern;

public record ReplMethod(String className, String methodName) {

  public String computePackageName() {
    val lastDot = this.className.lastIndexOf('.');
    if (lastDot >= 0) {
      return this.className.substring(0, lastDot);
    } else {
      return "";
    }
  }

  private static final Pattern INNER_CLASS_PATTERN =
      Pattern.compile("^(.+?\\.[A-Z]\\w+)\\.([A-Z]\\w+)$");

  public static ReplMethod fromRef(final String input) {
    final var sanitizedInput = input.replaceAll("\\([^)]*\\)", "");
    final var parts = sanitizedInput.split("#");
    if (parts.length != 2) {
      throw new IllegalArgumentException(
          "Cannot parse <className>#<methodName> from `" + input + "`");
    }

    var className = parts[0].trim();
    final var matcher = INNER_CLASS_PATTERN.matcher(className);
    if (matcher.matches()) {
      className = matcher.group(1) + "$" + matcher.group(2);
    }
    return new ReplMethod(className, parts[1].trim());
  }
}
