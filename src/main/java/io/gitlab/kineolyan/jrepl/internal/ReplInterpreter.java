package io.gitlab.kineolyan.jrepl.internal;

import java.util.regex.Pattern;

public class ReplInterpreter {

  public static ReplInterpreter create() {
    return new ReplInterpreter();
  }

  public Command interpret(final String input) {
    if ("help".equals(input)) {
      return new Command.PrintHelp();
    } else if ("!!".equals(input)) {
      return new Command.RunHistory(1);
    } else if (Pattern.compile("!\\d+").matcher(input).matches()) {
      final var matcher = Pattern.compile("!(\\d+)").matcher(input);
      matcher.matches();
      final int position = Integer.parseInt(matcher.group(1));
      return new Command.RunHistory(position);
    } else if ("e*".equals(input)) {
      return new Command.ShowLastError();
    } else if (Pattern.compile("e\\d+").matcher(input).matches()) {
      final var matcher = Pattern.compile("e(\\d+)").matcher(input);
      matcher.matches();
      final int position = Integer.parseInt(matcher.group(1));
      return new Command.ShowResult(position);
    } else if ("v*".equals(input)) {
      return new Command.ListVariables();
    } else if ("v!".equals(input)) {
      return new Command.UndefVariables();
    } else if (input.startsWith("test:")) {
      final var method = ReplTest.fromRef(input.substring(5));
      return new Command.RunTest(method);
    } else if (input.startsWith("test-package:")) {
      final var method = new ReplTest.TestedPackage(input.substring("test-package".length()));
      return new Command.RunTest(method);
    } else if (input.startsWith("exec:")) {
      final var method = ReplMethod.fromRef(input.substring(5));
      return new Command.Evaluate(method);
    } else {
      final var method = ReplMethod.fromRef(input);
      return new Command.Evaluate(method);
    }
  }
}
