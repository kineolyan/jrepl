package io.gitlab.kineolyan.jrepl.internal;

import lombok.val;

public sealed interface ReplTest
    permits ReplTest.TestedPackage, ReplTest.TestedClass, ReplTest.TestedMethod {

  record TestedPackage(String packageName) implements ReplTest {}

  record TestedClass(String className) implements ReplTest {}

  record TestedMethod(String className, String methodName) implements ReplTest {

  }

  static ReplTest fromRef(final String input) {
    if (input.contains("#")) {
      // Must match a test name
      val ref = ReplMethod.fromRef(input);
      return new TestedMethod(ref.className(), ref.methodName());
    } else {
      return new TestedClass(input);
    }
  }
}
