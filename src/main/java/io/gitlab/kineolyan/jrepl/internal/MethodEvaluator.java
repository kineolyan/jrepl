package io.gitlab.kineolyan.jrepl.internal;

import io.gitlab.kineolyan.jrepl.ReplServer;
import io.gitlab.kineolyan.jrepl.ReplValue;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MethodEvaluator {

  private static final Logger logger = Logger.getLogger(ReplEvaluator.class.getSimpleName());

  public Object evaluate(final ReplSession session, final ReplMethod method) {
    final var executor = createExecutor(method);
    return executor.apply(session);
  }

  private Function<ReplSession, Object> createExecutor(final ReplMethod targetMethod) {
    final Class<?> targetClass;
    try {
      targetClass = Class.forName(targetMethod.className());
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    }

    final var runnableMethod =
        findRunnableMethod(targetMethod, targetClass)
            .orElseThrow(
                () ->
                    new IllegalArgumentException(
                        "Cannot run test " + targetMethod + ". No no-arg form"));
    makeAccessibleIfNeeded(runnableMethod);
    if (Modifier.isStatic(runnableMethod.getModifiers())) {
      return createStaticExecutor(runnableMethod);
    } else {
      final var constructor =
          findNoopConstructor(targetClass)
              .orElseThrow(
                  () ->
                      new IllegalArgumentException(
                          "No no-arg constructor to create a dummy instance to invoke the test "
                              + targetMethod));
      return createInstanceExecutor(constructor, runnableMethod);
    }
  }

  private static Optional<Method> findRunnableMethod(
      final ReplMethod input, final Class<?> targetClass) {
    return Arrays.stream(targetClass.getDeclaredMethods())
        .filter(m -> m.getName().equals(input.methodName()))
        .filter(m -> m.getParameterCount() == 0)
        .findFirst();
  }

  private static Optional<Constructor<?>> findNoopConstructor(Class<?> targetClass) {
    return Arrays.stream(targetClass.getConstructors())
        .filter(c -> c.getParameterCount() == 0)
        .findFirst();
  }

  private static void makeAccessibleIfNeeded(Method runnableMethod) {
    // TODO: with JRebel, even with public modifiers, it seems that we have to make it accessible.
    // See why
    //        if (!Modifier.isPublic(runnableMethod.getModifiers())) {
    if (runnableMethod.trySetAccessible()) {
      logger.log(
          Level.FINER, "Method {0} made accessible to the REPL", new Object[] {runnableMethod});
    } else {
      throw new IllegalArgumentException(
          "Method not accessible to the REPL. Consider opening it the REPL module "
              + ReplServer.class.getModule());
    }
    //        }
  }

  private static Function<ReplSession, Object> createStaticExecutor(Method runnableMethod) {
    return (session) -> {
      try {
        final var result = runnableMethod.invoke(null);
        if (runnableMethod.isAnnotationPresent(ReplValue.class)) {
          final var annotation = runnableMethod.getAnnotation(ReplValue.class);
          final var variableName = annotation.value();
          if (variableName == null || variableName.isBlank()) {
            logger.log(
                Level.WARNING,
                "Method {0} defined with {1} but does not define a valid variable name",
                new Object[] {runnableMethod, ReplValue.class.getSimpleName()});
          } else if (annotation.once() && session.defined(variableName)) {
            logger.log(
                Level.FINER,
                "Variable {0} created by {1} is already defined, skipping",
                new Object[] {variableName, runnableMethod});
          } else {
            session.def(variableName, result);
          }
        }
        return result;
      } catch (IllegalAccessException | InvocationTargetException e) {
        throw new RuntimeException("Cannot invoke " + runnableMethod, e);
      }
    };
  }

  private static Function<ReplSession, Object> createInstanceExecutor(
      final Constructor<?> constructor, final Method runnableMethod) {
    return session -> {
      final Object instance;
      instance = createObject(constructor);
      return invoke(runnableMethod, instance);
    };
  }

  private static Object createObject(final Constructor<?> constructor) {
    try {
      return constructor.newInstance();
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
      throw new RuntimeException("Failed to create instance", e);
    }
  }

  private static Object invoke(final Method method, final Object instance) {
    try {
      return method.invoke(instance);
    } catch (IllegalAccessException | InvocationTargetException e) {
      throw new RuntimeException("Failed to invoke test", e);
    }
  }
}
