package io.gitlab.kineolyan.jrepl.internal;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;

class TestRingBuffer {

  @Test
  void testEmpty() {
    final var buffer = RingBuffer.ofSize(10);
    assertThat(buffer).isEmpty();
    assertThat(buffer).hasSize(0);
  }

  @Test
  void testInsert() {
    final var buffer = buffer(10, 5, 3);
    assertThat(buffer).hasSize(3);
  }

  @Test
  void testGet() {
    final var buffer = buffer(10, 5);
    assertThat(buffer.get(0)).isEqualTo(10);
    assertThat(buffer.get(1)).isEqualTo(5);
  }

  @Test
  void testLast() {
    final var buffer = buffer(10, 5);
    assertThat(buffer.last(1)).isEqualTo(5);
    assertThat(buffer.last(2)).isEqualTo(10);
  }

  @Test
  void testDefaultLast() {
    final var buffer = buffer(10, 5);
    assertThat(buffer.last()).isEqualTo(buffer.last(1));
  }

  private static RingBuffer<Integer> buffer(final int... values) {
    final var buffer = RingBuffer.<Integer>ofSize(10);
    IntStream.of(values).boxed().forEach(buffer::push);
    return buffer;
  }
}
